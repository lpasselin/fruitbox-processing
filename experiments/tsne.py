import torch
import numpy as np

from MulticoreTSNE import MulticoreTSNE as TSNE

from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt


rendements = np.array([
    0.264550265,
    0.29587766,
    0.275930851,
    0.318396226,
    0.242440042,
    0.2890625,
    0.224098887,
    0.299528302,
    0.280898876,
    0.319912609,
    0.280554508,
    0.274500624,
    0.280898876,
    0.257512758,
    0.257157021,
    0.205729167,
    0.238207547,
    0.291666667,
    0.231875,
    0.326766981,
])


batch_number = torch.load('/home/louis/p/fruitbox-processing/data/experiments/batch_number.pt').numpy()
latent_vector = torch.load('/home/louis/p/fruitbox-processing/data/experiments/latent_vector.pt').numpy()

mask = batch_number != -1
batch_number = batch_number[mask]
latent_vector = latent_vector[mask, :]
batch_rendement = rendements[batch_number]

X = latent_vector
tsne = TSNE(n_jobs=3, n_components=2, perplexity=10, n_iter=5000, learning_rate=200)
Y = tsne.fit_transform(X)

fig = plt.figure(figsize=(20, 20), dpi=200)
# ax = fig.add_subplot(111, projection='3d')
ax = fig.add_subplot(111)
max_c = np.ceil(batch_rendement.max())

# a = ax.scatter(Y[:, 0], Y[:, 1], Y[:, 2], c=batch_rendement, cmap=plt.cm.get_cmap("jet", max_c), marker='.')
# a = ax.scatter(Y[:, 0], Y[:, 1], Y[:, 2], c=batch_rendement, marker='.')
a = ax.scatter(Y[:, 0], Y[:, 1], c=batch_number, marker='.')

# fig.colorbar(a, ticks=range(max_c))
fig.colorbar(a)
# plt.clim(-0.5, max_c - 0.5)

plt.show()
print('t-SNE done')


