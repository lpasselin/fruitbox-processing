from torch import nn
import torch


class CppnDecoder(nn.Module):
    def __init__(self, z_size, fc_ch=128, img_channels=4, img_shape=(256, 256), device='cuda'):
        super(CppnDecoder, self).__init__()

        assert img_shape[0] == img_shape[1]

        self.img_channels = img_channels
        self.img_shape = img_shape
        self.total_pixels = img_shape[0] ** 2

        self.criterion = nn.L1Loss()

        ch_in = z_size + 3
        self.model = nn.Sequential(nn.Linear(ch_in, fc_ch), nn.BatchNorm1d(fc_ch), nn.SELU(inplace=True),
                                   nn.Linear(fc_ch, self.img_channels),
                                   nn.Sigmoid(), )


        xx, yy = torch.meshgrid([torch.linspace(-1, 1, img_shape[0]), torch.linspace(-1, 1, img_shape[0])])
        xx = xx.contiguous().view(-1).view(-1, 1)
        yy = yy.contiguous().view(-1).view(-1, 1)
        r = torch.sqrt(xx**2 + yy**2)
        self.coordinates = torch.cat((xx, yy, r), dim=1).contiguous()
        self.coordinates = self.coordinates.to(device)

        assert self.coordinates.nelement() / 3 == self.total_pixels
        assert self.coordinates.shape[1] == 3

    def forward(self, x, coordinates=None):
        batch_size = x.shape[0]
        x_all_pixels = x \
            .unsqueeze(1) \
            .expand(-1, self.total_pixels, -1)

        batch_coord = self.coordinates \
            .view(1, self.total_pixels, 3) \
            .expand(batch_size, -1, -1)

        x = torch.cat((batch_coord, x_all_pixels), dim=-1)
        x = x.view(batch_size * self.total_pixels, -1)

        y = self.model(x)
        y = y.view(batch_size, self.total_pixels, self.img_channels)
        y = y.view(batch_size, *self.img_shape, self.img_channels)
        y = y.permute(0, 3, 1, 2)
        return y

    def inference(self, x, img_channels=4, img_shape=(256, 256), device='cuda'):
        total_pixels = img_shape[0]**2

        xx, yy = torch.meshgrid([torch.linspace(-1, 1, img_shape[0]), torch.linspace(-1, 1, img_shape[0])])
        xx = xx.contiguous().view(-1).view(-1, 1)
        yy = yy.contiguous().view(-1).view(-1, 1)
        r = torch.sqrt(xx**2 + yy**2)
        coordinates = torch.cat((xx, yy, r), dim=1).contiguous()
        coordinates = coordinates.to(device)
        x = x.unsqueeze(0)

        batch_size = 1
        x_all_pixels = x \
            .unsqueeze(1) \
            .expand(-1, total_pixels, -1)

        batch_coord = coordinates \
            .view(1, total_pixels, 3) \
            .expand(batch_size, -1, -1)

        x = torch.cat((batch_coord, x_all_pixels), dim=-1)
        x = x.view(batch_size * total_pixels, -1)
        y = self.model(x)
        y = y.view(batch_size, total_pixels, img_channels)
        y = y.view(batch_size, *img_shape, img_channels)
        y = y.permute(0, 3, 1, 2)
        return y




if __name__ == "__main__":
    from torchsummary import summary

    z_size = 128
    summary(model=CppnDecoder(z_size, device='cuda').to('cuda'),
            input_size=(z_size,))
