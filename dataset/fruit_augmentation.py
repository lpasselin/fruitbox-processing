import numpy as np
import torch

class FruitAugmentation:
    def __call__(self, imgs):
        # random rotation (4 possible cases)
        r = np.random.randint(4)
        # random flip row axis (2 possible cases)
        flip1 = np.random.randint(2)
        # random flip col axis (2 possible cases)
        flip2 = np.random.randint(2)

        if r == 0:
            pass
        elif r == 1:
            # 90 deg
            imgs = imgs.transpose(-2, -1).flip(-2)
            imgs = torch.cat((imgs[3:12, :, :],
                              imgs[0:3, :, :],
                              imgs[12, :, :].unsqueeze(0)), dim=0)
        elif r == 2:
            # 180 deg
            imgs = imgs.flip(-1).flip(-2)
            imgs = torch.cat((imgs[6:12, :, :],
                              imgs[0:6, :, :],
                              imgs[12, :, :].unsqueeze(0)), dim=0)
        elif r == 3:
            # 270 deg
            imgs = imgs.transpose(-1, -2).flip(-1)
            imgs = torch.cat((imgs[9:12, :, :],
                              imgs[0:9, :, :],
                              imgs[12, :, :].unsqueeze(0)), dim=0)

        # random mirror rows (2 possible cases)
        if flip1 == 0:
            pass
        elif flip1 == 1:
            imgs = imgs.flip(-2)
            imgs = torch.cat((imgs[0:3, :, :],
                              imgs[9:12, :, :],
                              imgs[6:9, :, :],
                              imgs[3:6, :, :],
                              imgs[12, :, :].unsqueeze(0)), dim=0)

        if flip2 == 0:
            pass
        elif flip2 == 1:
            imgs = imgs.flip(-1)
            imgs = torch.cat((imgs[6:9, :, :],
                              imgs[3:6, :, :],
                              imgs[0:3, :, :],
                              imgs[9:12, :, :],
                              imgs[12, :, :].unsqueeze(0)), dim=0)

        return imgs
