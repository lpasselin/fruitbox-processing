import os
import numpy as np
import torch
import torch.utils.data as data
from torchvision import transforms
import torchvision.transforms.functional as F
from torchvision.datasets.folder import pil_loader
import ezexr
from skimage.io import imsave
import uuid
import json
from collections import OrderedDict
import copy
import random
import numpy as np

N_LIGHTS = 4

class FruitLoader(data.DataLoader):
    def __init__(self, *args, **kwargs):
        super(FruitLoader, self).__init__(*args, **kwargs)

class Fruit:
    def __init__(self, uid, pic_id, seq_id, root, imgs=None, mask=None):
        self._imgs = imgs
        self._mask = mask
        self.uid = uid
        self.seq_id = seq_id
        self.pic_id = pic_id
        self.root = root

    def clear(self):
        self._imgs = None
        self._mask = None

    def _is_imgs_loaded(self):
        return type(self._imgs) != type(None)

    def _is_mask_loaded(self):
        return type(self._mask) != type(None)

    @property
    def img_random_light(self):
        i = np.random.randint(N_LIGHTS)
        path = os.path.join(self.root, "data", self.pic_id, "{}-{}.exr".format(self.seq_id, i))
        path_mask = os.path.join(self.root, "data", self.pic_id, "{}m.png".format(self.seq_id))
        img = F.to_tensor(ezexr.imread(path)[:, :, 0:3].astype(np.float32))
        mask = pil_loader(path_mask)
        mask = F.to_tensor(mask.convert('1'))
        img *= mask.view(1, *img.shape[1:])

        # apply rotation transformation
        if i == 0:
            pass
        elif i == 1:
            # 90 deg
            img = img.transpose(-2, -1).flip(-2)
        elif i == 2:
            # 180 deg
            img = img.flip(-1).flip(-2)
        elif i == 3:
            # 270 deg
            img = img.transpose(-1, -2).flip(-1)

        return img

    @property
    def imgs(self):
        if not self._is_imgs_loaded():
            imgs = []
            for i in range(N_LIGHTS):
                path = os.path.join(self.root, "data", self.pic_id, "{}-{}.exr".format(self.seq_id, i))
                imgs.append(F.to_tensor(ezexr.imread(path)[:,:,0:3].astype(np.float32)))
            imgs = torch.stack(imgs)  # shape out (4, 3, y, x)
            self._imgs = imgs.view((-1, *imgs.shape[2:]))  # shape out (4*3, y, x)
        return self._imgs

    @property
    def mask(self):
        if not self._is_mask_loaded():
            mask = pil_loader(os.path.join(self.root, "data", self.pic_id, "{}m.png".format(self.seq_id)))
            mask = F.to_tensor(mask.convert('1'))
            self._mask = mask
        return self._mask

    def save(self):
        assert self._is_imgs_loaded() and self._is_mask_loaded()

        folder = os.path.join(self.root, "data", str(self.pic_id))
        if not os.path.exists(folder):
            os.makedirs(folder)
        for i in range(N_LIGHTS):
            ezexr.imwrite(os.path.join(folder, "{}-{}.exr".format(self.seq_id, i)), \
                    np.float16(self._imgs[i].transpose(0, 2).transpose(0, 1)))
        imsave(os.path.join(folder, "{}m.png".format(self.seq_id)), self._mask)
        self.clear()

class LRU(OrderedDict):
    """
    Keep track of loaded `Fruit` in memory and remove the first added if the
    limit is exceed.
    """
    def __init__(self, max_size):
        self.max_size = max_size
        super().__init__()

    def add(self, elem):
        self[elem] = None
        if len(self) > self.max_size:
            oldest = next(iter(self))
            oldest.clear()
            del self[oldest]

    def __repr__(self):
        return str(self)

    def __str__(self):
        return "LRU([{}], maxsize={}]".format( \
                ', '.join([str(k) for k in self.keys()]), self.max_size)

class CropFruitDataset(data.Dataset):

    def __init__(self, root, max_size=128, dataset_type="data", transforms=[]):
        # List containing all fruit unique id
        self.uids = []

        # The following dictionary are all indexed by uid
        # `Fruit` object
        self.data = {}
        # Picture id (e.i. folder name)
        self.pic_ids = {}
        # Sequence id (e.i. index in one picture)
        self.seq_ids = {}
        # List of metadata for every `Fruit` (e.i. position, sugar, palette_id, ...)
        self.metadata = {}
        # Metadata labels
        self.labels = []

        self.root = root
        self.max_size = max_size
        self.fruit_loaded = LRU(self.max_size)

        self.transforms = transforms

        assert dataset_type in ["data", "train", "valid"]
        self.type = dataset_type

    def add(self, imgs, mask, pic_id, seq_id, metadata=[]):
        uid = uuid.uuid4().int & 0xFFFFFFFF
        fruit = Fruit(uid, pic_id, seq_id, self.root, imgs, mask)

        self.uids.append(uid)
        self.data[uid] = fruit
        self.pic_ids[uid] = str(pic_id)
        self.seq_ids[uid] = str(seq_id)
        self.metadata[uid] = metadata
        self.fruit_loaded.add(fruit)

    def save(self):
        data = {}
        data["pic_ids"] = self.pic_ids
        data["seq_ids"] = self.seq_ids
        data["uids"] = self.uids
        data["metadata"] = self.metadata
        data["labels"] = self.labels

        for fruit in self.fruit_loaded:
            fruit.save()
        self.fruit_loaded = LRU(max_size=self.max_size)
        with open(os.path.join(self.root, "{}.json".format(self.type)), 'w') as f:
            json.dump(data, f)

    def load(self):
        with open(os.path.join(self.root, "{}.json".format(self.type)), 'r') as f:
            data = json.load(f)
        self.pic_ids = {int(k):v for k, v in data["pic_ids"].items()}
        self.seq_ids = {int(k):v for k, v in data["seq_ids"].items()}
        self.metadata = {int(k):v for k, v in data["metadata"].items()}
        self.labels = data["labels"]
        self.uids = data["uids"]
        for uid in self.uids:
            self.data[uid] = Fruit(uid, self.pic_ids[uid], self.seq_ids[uid], self.root)

    def split(self, ratio=0.8):
        assert len(self.fruit_loaded) == 0
        random.shuffle(self.uids)
        index = int(ratio*len(self.uids))

        train_dataset = CropFruitDataset(self.root, max_size=self.max_size, dataset_type="train")
        train_dataset.uids = self.uids[:index]
        valid_dataset = CropFruitDataset(self.root, max_size=self.max_size, dataset_type="valid")
        valid_dataset.uids = self.uids[index:]
        for dataset in [valid_dataset, train_dataset]:
            dataset.labels = self.labels
            for uid in dataset.uids:
                dataset.pic_ids[uid] = self.pic_ids[uid]
                dataset.seq_ids[uid] = self.seq_ids[uid]
                dataset.metadata[uid] = self.metadata[uid]
                dataset.data[uid] = self.data[uid]

        return train_dataset, valid_dataset

    def random(self):
        return self.get_by_uid(random.choice(self.uids))

    def get_by_uid(self, uid):
        fruit = self.data[uid]
        self.fruit_loaded.add(fruit)
        # img = torch.cat((fruit.imgs, fruit.mask))
        img = fruit.img_random_light
        for transform in self.transforms:
                img = transform(img)
        return img, [img, uid]

    def __getitem__(self, index):
        return self.get_by_uid(self.uids[index])

    def __iter__(self):
        for uid in self.uids:
            yield self.get_by_uid(uid)

    def __len__(self):
        return len(self.data)

class SegmentationFruitDataset(data.Dataset):

    def __init__(self, root):
        # TODO: metadata from image_info.csv

        self.root = root
        self.data_dirs = [os.path.join(root, d) for d in os.listdir(root)]
        self.data_dirs = sorted([d for d in self.data_dirs if os.path.isdir(d)])

        if len(self.data_dirs) == 0:
            raise (ValueError("Found 0 files."))

    def read_imgs(self, dir):
        imgs_paths = sorted([os.path.join(dir, f) for f in os.listdir(dir)])
        imgs = [F.to_tensor(ezexr.imread(p)[:,:,0:3].astype(np.float32)) for p in imgs_paths if p.endswith(".exr")]
        imgs = torch.stack(imgs)  # shape out (4, 3, 720, 1080)
        return imgs

    def read_mask(self, dir):
        img = pil_loader(os.path.join(dir, "mask.png"))
        img = F.to_tensor(img.convert('1'))
        return img

    def __getitem__(self, index):
        dir = self.data_dirs[index]
        X = self.read_imgs(dir)
        Y = self.read_mask(dir)
        return X, Y

    def __len__(self):
        return len(self.data_dirs)

if __name__ == "__main__":
    db = CropFruitDataset("../crop", dataset_type="data")
    db.load()

    db2 = CropFruitDataset("../crop", dataset_type="data")

    for uid in db.uids:
        id = uid & 0xFFFFFFFF
        db2.uids.append(id)
        db2.data[id] = db.data[uid]
        db2.pic_ids[id] = db.pic_ids[uid]
        db2.seq_ids[id] = db.seq_ids[uid]
        db2.metadata[id] = db.metadata[uid]
    db2.save()
    train, valid = db2.split()
    train.save()
    print(len(train))
    valid.save()
    print(len(valid))

