import csv
import re

from dataset import CropFruitDataset

img_to_data = {}
with open('dataset/prediction_data.csv') as f:
    csv_data = csv.reader(f, delimiter=',', quotechar='"')
    index = 0
    for r in list(csv_data):
        if r[1] == "Images":
            labels = r[2:]
            continue
        if r[2] == 'Cibles':
            break
        imgs = r[1]
        imgs = re.findall(r'\(IMG_([\d]+),IMG_([\d]+)\)', r[1])

        if imgs:
            row_data = r[2:]
            for i, val in enumerate(row_data):
                row_data[i] = eval(val)

        for start, stop in imgs:
            start = int(start)
            if start > 20000:
                start -= 1
            stop = int(stop)
            if stop > 20000:
                stop -= 1
            for val in range(start, stop+1):
                assert val not in img_to_data, val
                img_to_data[val] = row_data

img_to_palette = {}
with open('dataset/img_to_palette.csv') as f:
    csv_data = csv.reader(f, delimiter=',', quotechar='"')
    for r in csv_data:
        start, stop, palette = r[0:3]
        if re.findall(r'[\d]+', palette):
            start = int(start)
            stop = int(stop)
            palette = int(palette)

            for val in range(start, stop+1):
                assert val not in img_to_palette, val
                img_to_palette[val] = palette

dataset = CropFruitDataset("../crop")
dataset.load()

none_img = []
for uid in dataset.uids:
    try:
        palette_id = img_to_palette[int(dataset.pic_ids[uid])]
    except:
        palette_id = -1
    position = dataset.metadata[uid][1]

    try:
        metadata = [palette_id, position] + img_to_data[int(dataset.pic_ids[uid])]
    except:
        key = list(img_to_data.keys())[0]
        metadata = [palette_id, position] + [-1] * len(img_to_data[key])

    dataset.metadata[uid] = metadata
dataset.labels = ["Palette id", "Position"] + labels

for i, label in enumerate(dataset.labels):
    dataset.labels[i] = label.encode("ascii", errors="ignore").decode().strip()

print(none_img)

dataset.save()
train, valid = dataset.split()
train.save()
valid.save()
