import argparse
import os
from multiprocessing import cpu_count
import numpy as np

import torch
from torch import optim
from torch.utils import data
import torch.nn.functional as F

from dataset.dataset import FruitLoader, CropFruitDataset
from vae.net import AutoencoderVAE, Discriminator
from vae.callback import FruitCallback, TsneCallback
from vae.gan_callback import GanCallback

from pytorch_toolbox.train_loop import TrainLoop
from pytorch_toolbox.transformations.image import Normalize, NumpyImage2Tensor
from pytorch_toolbox.transformations.compose import Compose


class Resize:
    def __init__(self, scale_factor):
        self.scale_factor = scale_factor

    def __call__(self, imgs):
        imgs = F.interpolate(imgs.unsqueeze(0), scale_factor=self.scale_factor).squeeze(0)
        return imgs


class TonemapLog:
    def __init__(self, a=0.0001, b=0.1):
        # (log(img+a) - log(a)) / (log(b+a) - log(a))
        self.a = a
        self.b = b

    def __call__(self, imgs):
        a, b = self.a, self.b
        # do not apply on mask
        imgs[:12] = torch.log((imgs[:12] + a) / a) / np.log((b + a) / a)

        return imgs


class FruitMask:
    def __init__(self, inplace=True):
        self.inplace = inplace

    def __call__(self, imgs):
        # keep mask channel for compatibility with regular unmasked models
        mask = imgs[-1].unsqueeze(0).expand(imgs.shape)
        mask = mask != 0.0
        if self.inplace:
            imgs[~mask] = 0.0
        else:
            imgs = imgs * mask
        return imgs



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trainning VAEFruit')
    parser.add_argument('-o', '--output', help="Output path", required=True)
    parser.add_argument('-d', '--dataset', help="Dataset path", required=True)
    parser.add_argument('-l', '--learningrate', help="learning rate", action="store", default=0.001, type=float)
    parser.add_argument('-m', '--sharememory', help="Activate share memory", action="store_true")
    parser.add_argument('-b', '--loadbest', help="Load best model before training", action="store_true")
    parser.add_argument('-n', '--ncore', help="number of cpu core to use, -1 is all core", action="store", default=-1, type=int)
    parser.add_argument('-g', '--gradientclip', help="Activate gradient clip", action="store_true")
    parser.add_argument('-e', '--epoch', help="number of epoch", action="store", default=25, type=int)
    parser.add_argument('-k', '--backend', help="backend : cuda | cpu", action="store", default="cuda")
    parser.add_argument('-s', '--batchsize', help="Size of minibatch", action="store", default=64, type=int)
    parser.add_argument('-t', '--tensorboard', help="Activate tensorboard", action="store_true")
    parser.add_argument('--tensorboard_path', help="Tensorboard log path", default="./logs")
    parser.add_argument('--cppn', help="CPPN decoder model (scale invariance)", action="store_true")
    arguments = parser.parse_args()

    data_path = arguments.dataset
    output_path = arguments.output
    backend = arguments.backend
    batch_size = arguments.batchsize
    epochs = arguments.epoch
    use_shared_memory = arguments.sharememory
    number_of_core = arguments.ncore
    learning_rate = arguments.learningrate
    load_best = arguments.loadbest
    gradient_clip = arguments.gradientclip
    use_tensorboard = arguments.tensorboard
    tensorboard_path = arguments.tensorboard_path
    cppn = arguments.cppn

    if number_of_core == -1:
        number_of_core = cpu_count()

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    model = AutoencoderVAE(cppn=cppn)
    loader_class = FruitLoader

    optimizer_vae = optim.Adam(model.parameters(), lr=learning_rate, weight_decay=0)

    transformations = [Compose([
        Resize(0.50),
        TonemapLog(),
    ])]

    train_dataset = CropFruitDataset(data_path, max_size=batch_size, dataset_type="train", transforms=transformations)
    train_dataset.load()
    valid_dataset = CropFruitDataset(data_path, max_size=batch_size, dataset_type="valid", transforms=transformations)
    valid_dataset.load()

    train_loader = data.DataLoader(train_dataset,
                                   batch_size=batch_size,
                                   shuffle=True,
                                   num_workers=number_of_core,
                                   pin_memory=use_shared_memory,
                                   drop_last=True,
                                   )

    val_loader = data.DataLoader(valid_dataset,
                                 batch_size=batch_size,
                                 num_workers=number_of_core,
                                 pin_memory=use_shared_memory,
                                 )

    train_loop_handler = TrainLoop(model, train_loader, val_loader, [optimizer_vae], backend, gradient_clip,
                                   use_tensorboard=use_tensorboard, tensorboard_log_path=tensorboard_path)

    # GAN
    model_discriminator = Discriminator()
    optimizer_discriminator = optim.SGD(model_discriminator.parameters(), lr=learning_rate)
    gan_callback = GanCallback(optimizer_discriminator, optimizer_vae, model_discriminator)

    # train_loop_handler.add_callback([FruitCallback(), gan_callback])
    train_loop_handler.add_callback([FruitCallback()])
    train_loop_handler.loop(epochs, output_path, save_all_checkpoints=False, save_last_checkpoint=True, load_best_checkpoint=load_best)

    print("Training Complete")
