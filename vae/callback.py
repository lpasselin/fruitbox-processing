import os
import numpy as np
import torch
import torch.nn.functional as F
from tqdm import tqdm


from pytorch_toolbox.loop_callback_base import LoopCallbackBase
from pytorch_toolbox.train_state import TrainingState
from pytorch_toolbox.train_loop import TrainLoop

from pytorch_toolbox.utils import AverageMeter

from sklearn.manifold import TSNE

from experiments.embedding_tensorboard import add_embedding

import matplotlib

matplotlib.use('agg')
import matplotlib.pyplot as plt

plt.switch_backend('agg')


class FruitCallback(LoopCallbackBase):
    def __init__(self):
        super().__init__()
        self.loss_latent_vector_avg = AverageMeter()
        self.loss_recons_content_avg = AverageMeter()

    def batch(self, state: TrainingState):
        if state.tensorboard_logger and state.training_mode:
            self.loss_latent_vector_avg.update(state.model.loss_recons_content.item())
            state.tensorboard_logger.scalar_summary('loss_recons (batch)', state.model.loss_recons_content.item(), state.current_batch)
            state.tensorboard_logger.scalar_summary('loss_recons_mask (batch)', state.model.loss_recons_mask.item(), state.current_batch)
            self.loss_recons_content_avg.update(state.model.loss_latent_vector.item())
            state.tensorboard_logger.scalar_summary('loss_latent_vector (batch)', state.model.loss_latent_vector.item(), state.current_batch)

    def epoch(self, state: TrainingState):
        if state.tensorboard_logger:
            if state.training_mode:
                state.tensorboard_logger.scalar_summary('loss_recons (epoch)', self.loss_recons_content_avg.avg, state.current_batch)
                self.loss_latent_vector_avg = AverageMeter()
                state.tensorboard_logger.scalar_summary('loss_latent_vector (epoch)', self.loss_latent_vector_avg.avg, state.current_batch)
                self.loss_recons_content_avg = AverageMeter()

            self.img_compare(state)

        self.print_batch_data(state)
        self.save_epoch_data("", state)

    def img_compare(self, state: TrainingState):
        with torch.no_grad():
            predict_imgs = []
            input_imgs = []
            for i in range(6):
                predict_imgs.append(state.last_prediction[0][i][:3].detach().cpu().numpy())
                input_imgs.append(state.last_network_input[0][i].detach().cpu().numpy())
            predict_imgs = np.concatenate(predict_imgs, axis=2)
            input_imgs = np.concatenate(input_imgs, axis=2)
            imgs = np.concatenate((predict_imgs, input_imgs), axis=1)

            state.tensorboard_logger.image_summary("compare", [imgs], state.current_epoch, is_train=state.training_mode)

class BatchResize:
    def __init__(self, scale_factor):
        self.scale_factor = scale_factor

    def __call__(self, imgs):
        imgs = F.interpolate(imgs, scale_factor=self.scale_factor)
        return imgs

class TsneCallback(LoopCallbackBase):
    def __init__(self, valid_dataset, valid_loader):
        super().__init__()
        self.valid_dataset = valid_dataset
        self.valid_loader = valid_loader

    def batch(self, state: TrainingState):
        pass
        # self.latent_vector_manifold_img(state, save_data=True)

    def epoch(self, state: TrainingState):
        self.latent_vector_manifold_img(state, save_data=True)

    def latent_vector_manifold_img(self, state: TrainingState, save_data=False):
        # import pydevd
        # pydevd.settrace('lpasselintrain.duckdns.org', port=15155, stdoutToServer=True, stderrToServer=True)

        model = state.model
        model.eval()
        latent_vector_list = []
        id_list = []
        sprite_list = []

        from torchvision.transforms import Compose
        sprite_transform = Compose([
            BatchResize(scale_factor=1/2**3),
        ])

        with torch.no_grad():
            for i, (data, target) in tqdm(enumerate(self.valid_loader), total=len(self.valid_loader)):
                data, target = TrainLoop.setup_loaded_data(data, target, 'cuda')
                data_var, target_var = TrainLoop.to_autograd(data, target, is_train=False)

                latent_vector = model.forward(data_var[0], latent_vector_only=True)
                latent_vector_list.append(latent_vector)
                id_list.append(target_var[1])

                sprites = sprite_transform(data_var[0])
                sprite_list.append(sprites)

        latent_vector = torch.cat(latent_vector_list, dim=0).cpu().numpy()
        id = torch.cat(id_list, dim=0).cpu().numpy()
        label_img = torch.cat(sprite_list, dim=0).cpu().numpy()

        # import pydevd
        # pydevd.settrace('lpasselintrain.duckdns.org', port=15155, stdoutToServer=True, stderrToServer=True)

        metadata = [self.valid_dataset.metadata[i][:] for i in id]

        # tsne save for tensorboard
        logdir = state.tensorboard_logger.writer_train.event_writer._logdir
        add_embedding(
            logdir=logdir,
            mat=latent_vector,
            metadata=metadata,
            label_img=label_img,
            step=state.current_epoch + state.current_batch,
            tag='embed_default',
            metadata_header=self.valid_dataset.labels,
        )

