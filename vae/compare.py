import argparse
import torch
import numpy as np
import matplotlib.pyplot as plt

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Random VAEFruit')
    parser.add_argument('-m1', '--model1', help="Model", required=True)
    parser.add_argument('-m2', '--model2', help="Model", required=True)
    parser.add_argument('-w', '--width', help="Width grid", required=True)
    parser.add_argument('-d', '--dataset', help="Dataset path", required=True)

    arguments = parser.parse_args()

    model_path_1 = arguments.model1
    model_path_2 = arguments.model2
    nb = int(arguments.width)
    data_path = arguments.dataset

    net1 = AutoencoderVAE()
    net1.load(model_path_1)
    net1.eval()

    net2 = AutoencoderVAE()
    net2.load(model_path_2)
    net2.eval()

    transformations = [Compose([
        Resize(0.50),
        FruitMask(inplace=True),
        TonemapLog(),
    ])]

    valid_dataset = CropFruitDataset(data_path, max_size=32, dataset_type="valid", transforms=transformations)
    valid_dataset.load()

    cherries_in = [valid_dataset.random()[0] for _ in range(nb)]
    cherries_1 = np.concatenate(
            [net1(cherry.unsqueeze(0))[0][0,:3,:,:].detach().numpy() \
                    for cherry in cherries_in], axis=2)
    cherries_2 = np.concatenate(
            [net2(cherry.unsqueeze(0))[0][0,:3,:,:].detach().numpy() \
                    for cherry in cherries_in], axis=2)
    cherries_in = np.concatenate(
            [cherry.detach().numpy() for cherry in cherries_in], axis=2)


    imgs = np.concatenate((cherries_in, cherries_1, cherries_2), axis=1)
    imgs = np.clip(imgs.swapaxes(0, 2).swapaxes(0, 1), 0, 1)

    plt.imsave('compare', imgs)
