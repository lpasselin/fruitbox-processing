import argparse
import numpy as np
import torch

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

import matplotlib.pyplot as plt

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interpolation VAEFruit')
    parser.add_argument('-m', '--model', help="Model", required=True)
    parser.add_argument('-d', '--dataset', help="Dataset path", required=True)

    arguments = parser.parse_args()

    model_path = arguments.model
    data_path = arguments.dataset

    net = AutoencoderVAE()
    net.load(model_path)
    net.eval()

    transformations = [Compose([
        Resize(0.50),
        FruitMask(inplace=True),
        TonemapLog(),
    ])]

    valid_dataset = CropFruitDataset(data_path, max_size=32, dataset_type="valid", transforms=transformations)
    valid_dataset.load()

    cherries = [valid_dataset.random()[0] for _ in range(4)]
    latents_vec = torch.cat(
        [net.encoder(cherry.unsqueeze(0))[0] for cherry in cherries]
    )

    nb = 5
    latents_vec = latents_vec.permute(1, 0).view(128, 2, 2).unsqueeze(0)
    latent_interpolate = torch.nn.functional.interpolate(latents_vec, \
            size=(nb, nb), mode='bilinear', align_corners=True).squeeze(0)

    img_h = []
    for i in range(nb):
        img_w = []
        for j in range(nb):
            latent = latent_interpolate[:,i,j].unsqueeze(0)
            img = net.decoder(latent)[0,:3,:,:].detach().numpy()
            img_w.append(img)
        img_h.append(np.concatenate(img_w, axis=2))
    imgs = np.concatenate(img_h, axis=1)

    left_size = [cherries[0]] + [torch.ones_like(cherries[0]) for _ in range(nb-2)] + [cherries[2]]
    left_size = np.concatenate([img.detach().numpy() for img in left_size], axis=1)
    right_size = [cherries[1]] + [torch.ones_like(cherries[0]) for _ in range(nb-2)] + [cherries[3]]
    right_size = np.concatenate([img.detach().numpy() for img in right_size], axis=1)
    imgs = np.concatenate((left_size, imgs, right_size), axis=2)
    imgs = np.clip(imgs.swapaxes(0, 2).swapaxes(0, 1), 0, 1)

    plt.imsave('interpolate', imgs)
