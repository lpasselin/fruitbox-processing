import argparse
import numpy as np
import matplotlib.pyplot as plt

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

from PIL import Image
import torchvision.transforms.functional as F

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Random VAEFruit')
    parser.add_argument('-m', '--model', help="Model", required=True)
    parser.add_argument('-i', '--input', help="input img", required=True)
    arguments = parser.parse_args()

    model_path = arguments.model
    input_path = arguments.input

    img_in = Image.open(input_path)
    res = F.to_tensor(img_in)
    img_in = res[:3,:,:]

    net = AutoencoderVAE()
    net.load(model_path)
    net.eval()

    res = net(img_in.unsqueeze(0))[0]
    img = res[0,:3].detach().numpy()
    mask = res[0,3].detach().numpy()
    img[:, mask < 0.5] = 1
    img = np.clip(img.swapaxes(0, 2).swapaxes(0, 1), 0, 1)
    plt.imsave('res', img)
