import os
import numpy as np
import torch
import torch.nn as nn

from pytorch_toolbox.loop_callback_base import LoopCallbackBase
from pytorch_toolbox.train_state import TrainingState

from pytorch_toolbox.utils import AverageMeter

class GanCallback(LoopCallbackBase):
    def __init__(self, optimizer_disc, optimizer_vae, net):
        super().__init__()
        self.optimizer_disc = optimizer_disc
        self.optimizer_vae = optimizer_vae
        self.net = net.cuda()

    def batch(self, state: TrainingState):
        # GAN train loop
        if state.training_mode:
            self.net.train()
            self.optimizer_disc.zero_grad()

            # Loss Generator
            generator = self.net(state.last_prediction[0][:,:3,:,:])
            loss_generator_gan = self.net.loss_generator(generator) * 1e2
            # loss_generator_gan = self.net.loss_generator(fake)

            # Loss Discriminator
            targets = state.last_network_input[0]
            noise = torch.randn(targets.size(0), state.model.latent_length).to( \
                    targets.device)
            noise.requires_grad = True
            predictions = state.model.decoder(noise)
            real = self.net(state.last_network_input[0][:,:3,:,:])
            fake = self.net(predictions[:,:3,:,:])
            loss_real_disc, loss_fake_disc = self.net.loss_gan(fake, real)
            losses_disc = loss_real_disc + loss_fake_disc

            # Update Discriminator
            losses_disc.backward(retain_graph=True)
            self.optimizer_disc.step()

            self.optimizer_vae.zero_grad()

            # Update Generator
            loss_generator_gan.backward(retain_graph=True)
            self.optimizer_vae.step()
            self.optimizer_vae.zero_grad()


            if state.tensorboard_logger:
                state.tensorboard_logger.scalar_summary('loss_real_gan (batch)', loss_real_disc.item(), state.current_batch)
                state.tensorboard_logger.scalar_summary('loss_fake_gan (batch)', loss_fake_disc.item(), state.current_batch)
                state.tensorboard_logger.scalar_summary('loss_generator_gan (batch)', loss_generator_gan.item()/1e2, state.current_batch)

    def epoch(self, state: TrainingState):
        pass
