import argparse
import numpy as np
import torch
from torch.utils import data

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

import matplotlib.pyplot as plt

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interpolation VAEFruit')
    parser.add_argument('-m', '--model', help="Model", required=True)
    parser.add_argument('-d', '--dataset', help="Dataset path", required=True)
    parser.add_argument('-n', '--nb', help="Number", required=True)

    arguments = parser.parse_args()

    model_path = arguments.model
    data_path = arguments.dataset
    nb = arguments.nb

    net = AutoencoderVAE()
    net.load(model_path)
    net.eval()
    net.cuda()

    transformations = [Compose([
        Resize(0.50),
        FruitMask(inplace=True),
        TonemapLog(),
    ])]

    valid_dataset = CropFruitDataset(data_path, max_size=32, dataset_type="valid", transforms=transformations)
    valid_dataset.load()

    result = {}
    for i, (img, (target, uid)) in enumerate(valid_dataset):
        print(i)
        res = net(img.cuda().unsqueeze(0))
        result[uid] = net.loss(res, target.cuda().unsqueeze(0)).item()

    net.cpu()
    for i, uid in enumerate(reversed(sorted(result, key=result.get))):
        if i >= int(nb):
            break
        img, _ = valid_dataset.get_by_uid(uid)
        res = net(img.unsqueeze(0))[0]
        mask = res[0,3].detach().numpy()
        res = res[0,:3].detach().numpy()
        res[:, mask < 0.5] = 1
        img = np.concatenate((img, res), axis=1)
        img = np.clip(img.swapaxes(0, 2).swapaxes(0, 1), 0, 1)
        plt.imsave('worst/{}'.format(i), img)
