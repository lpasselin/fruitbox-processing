import argparse
import torch
import numpy as np
import matplotlib.pyplot as plt

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Random VAEFruit')
    parser.add_argument('-m', '--model', help="Model", required=True)
    parser.add_argument('-w', '--width', help="Width grid", required=True)
    parser.add_argument('-e', '--height', help="Height grid", required=True)

    arguments = parser.parse_args()

    model_path = arguments.model
    width = int(arguments.width)
    height = int(arguments.height)

    net = AutoencoderVAE()
    net.load(model_path)

    noise = torch.randn(width*height, 128)
    imgs = net.decoder(noise)[:,:3,:,:]

    img_h = []
    for i in range(height):
        img_w = []
        for j in range(width):
            index = i*width + j
            img_w.append(imgs[index].detach().numpy())
        img_h.append(np.concatenate(img_w, axis=2))
    imgs = np.concatenate(img_h, axis=1)
    imgs = imgs.swapaxes(0, 2).swapaxes(0, 1)
    imgs = np.clip(imgs, 0, 1)

    plt.imsave('random', imgs)
