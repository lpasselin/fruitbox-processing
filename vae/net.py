import torch.nn.functional as F
import torch.nn as nn
from pytorch_toolbox.network_base import NetworkBase
# from pytorch_toolbox.modules.conv2d_module import ConvBlock
# from pytorch_toolbox.modules.fc_module import FCBlock
import torch
from torchvision.models import densenet121, densenet201, resnet18
import functools
import numpy as np

from experiments.cppn import CppnDecoder

def tv_loss(x):
    return torch.sum(torch.abs(x[:, :, :, :-1] - x[:, :, :, 1:])) + \
            torch.sum(torch.abs(x[:, :, :-1, :] - x[:, :, 1:, :]))

class Encoder(nn.Module):
    def __init__(self, channel_in=3, z_size=128):
        super(Encoder, self).__init__()
        layers_list = []
        # the first time 3->64, for every other double the channel size
        self.size = channel_in
        for i in range(5):
            if i == 0:
                layers_list.append(EncoderBlock(channel_in=self.size, channel_out=32))
                self.size = 32
            else:
                if i <= 2:
                    layers_list.append(EncoderBlock(channel_in=self.size, channel_out=self.size*2))
                    self.size *= 2
                else:
                    layers_list.append(EncoderBlock(channel_in=self.size, channel_out=self.size))

        # final shape Bx256x8x8
        self.conv = nn.Sequential(*layers_list)
        self.fc = nn.Sequential(nn.Linear(in_features=8 * 8 * self.size, out_features=1024, bias=False),
                                nn.BatchNorm1d(num_features=1024, momentum=0.9),
                                nn.ReLU(True))
        # two linear to get the mu vector and the diagonal of the log_variance
        self.l_mu = nn.Linear(in_features=1024, out_features=z_size)
        self.l_var = nn.Linear(in_features=1024, out_features=z_size)

    def forward(self, x):
        x = self.conv(x)
        x = x.view(len(x), -1)
        x = self.fc(x)
        mu = self.l_mu(x)
        logvar = self.l_var(x)
        return mu, logvar

class EncoderBlock(nn.Module):
    def __init__(self, channel_in, channel_out):
        super(EncoderBlock, self).__init__()
        # convolution to halve the dimensions
        self.conv = nn.Conv2d(in_channels=channel_in, out_channels=channel_out, \
                kernel_size=5, padding=2, stride=2, bias=False)
        self.bn = nn.BatchNorm2d(num_features=channel_out, momentum=0.9)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = F.relu(x, True)
        return x

class Decoder(nn.Module):
    def __init__(self, z_size, size):
        super(Decoder, self).__init__()
        # start from B*z_size
        self.fc = nn.Sequential(nn.Linear(in_features=z_size, out_features=8 * 8 * size, bias=False),
                                nn.BatchNorm1d(num_features=8 * 8 * size, momentum=0.9),
                                nn.ReLU(True), )
        self.size = size
        layers_list = []
        layers_list.append(DecoderBlock(channel_in=self.size, channel_out=self.size))
        layers_list.append(DecoderBlock(channel_in=self.size, channel_out=self.size//2))
        self.size = self.size//2
        layers_list.append(DecoderBlock(channel_in=self.size, channel_out=self.size//4))
        self.size = self.size//4
        layers_list.append(DecoderBlock(channel_in=self.size, channel_out=self.size//8))
        self.size = self.size//8
        layers_list.append(DecoderBlock(channel_in=self.size, channel_out=self.size))
        self.size = self.size
        # final conv to get 3 channels and tanh layer
        layers_list.append(nn.Sequential(
            nn.Conv2d(in_channels=self.size, out_channels=4, kernel_size=5, stride=1, padding=2),
            nn.Tanh()
        ))

        self.conv = nn.Sequential(*layers_list)

    def forward(self, x):

        x = self.fc(x)
        x = x.view(len(x), -1, 8, 8)
        x = self.conv(x)
        return x

class DecoderBlock(nn.Module):
    def __init__(self, channel_in, channel_out):
        super(DecoderBlock, self).__init__()
        # transpose convolution to double the dimensions
        self.conv = nn.ConvTranspose2d(channel_in, channel_out, kernel_size=5, padding=2, stride=2, output_padding=1,
                                       bias=False)
        self.bn = nn.BatchNorm2d(channel_out, momentum=0.9)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = F.relu(x, True)
        return x


class Discriminator(nn.Module):
    def __init__(self, channel_in=3):
        super(Discriminator, self).__init__()
        # module list because we need need to extract an intermediate output
	#nn.Conv2d(channel_in, 3, kernel_size=1)
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=channel_in, out_channels=32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(inplace=True),
            EncoderBlock(32, 64),
            EncoderBlock(64, 128),
            EncoderBlock(128, 128),
            EncoderBlock(128, 128),
            EncoderBlock(128, 128)
        )
        # final fc to get the score (real or fake)
        self.fc = nn.Sequential(
            nn.Linear(in_features=8 * 8 * 128, out_features=512, bias=False),
            nn.BatchNorm1d(num_features=512,momentum=0.9),
            nn.ReLU(inplace=True),
            nn.Linear(in_features=512, out_features=1),
        )

        self.criterion_real_disc = nn.MSELoss()
        self.criterion_fake_disc = nn.MSELoss()
        self.criterion_generator_disc = nn.MSELoss()

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        x = self.fc(x)
        return F.sigmoid(x)

    def loss_generator(self, predictions):
        # Generator loss
        generator_labels = torch.ones(predictions.size(0)).to(predictions.device)
        loss_generator = self.criterion_generator_disc(predictions, generator_labels)
        return loss_generator

    def loss_gan(self, predictions, targets):
        scramble_index = int(0.20*predictions.size(0))
        scramble_index = 0
        # Discriminator Loss
        real_labels = torch.cat((torch.ones(targets.size(0) - scramble_index), \
            torch.zeros(scramble_index))).to(targets.device)
        loss_real = self.criterion_real_disc(targets, real_labels)

        fake_labels = torch.cat((torch.zeros(predictions.size(0) - scramble_index), \
            torch.ones(scramble_index))).to(predictions.device)
        loss_fake = self.criterion_fake_disc(predictions, fake_labels)

        return loss_real, loss_fake

class AutoencoderVAE(NetworkBase):
    def __init__(self, in_features=3, latent_length=128,
                 loss_coef_recons_content=1e4, loss_coef_recons_mask=1e4, loss_coef_latent_vector=1e-4,
                 tv_loss_enable=False, cppn=False):
        super(AutoencoderVAE, self).__init__()
        self.latent_length = latent_length
        self.in_features = in_features
        self.tv_loss_enable = tv_loss_enable
        self.encoder = Encoder(in_features, latent_length)
        if cppn==False:
            self.decoder = Decoder(z_size=latent_length, size=self.encoder.size)
        else:
            self.decoder = CppnDecoder(z_size=latent_length)


        self.criterion_recons_content = nn.L1Loss()
        self.criterion_recons_mask = nn.L1Loss()
        self.criterion_latent_vector = LossGaussianVAE()
        self.loss_coef_recons_content = loss_coef_recons_content
        self.loss_coef_recons_mask = loss_coef_recons_mask
        self.loss_coef_latent_vector = loss_coef_latent_vector

    def forward(self, x, latent_vector_only=False):
        latent_vector = self.encoder(x)
        latent_mu, latent_logvar = latent_vector
        # TODO: validate this is ok to do with VAE (only keep mu)
        if self.training:
            new_latent_vector = self.reparameterize(latent_mu, latent_logvar)
        else:
            new_latent_vector = latent_mu

        if latent_vector_only:
            return new_latent_vector

        reconstruction = self.decoder(new_latent_vector)
        return reconstruction, latent_mu, latent_logvar

    def loss(self, predictions, targets):
        # import pydevd
        # pydevd.settrace('lpasselintrain.duckdns.org', port=15155, stdoutToServer=True, stderrToServer=True)
        if type(targets) == list:
            targets = targets[0]
        reconstruction, latent_mu, latent_logvar = predictions

        mask = reconstruction[:,3:,:,:]
        reconstruction = reconstruction[:,:3,:,:]

        self.loss_recons_content = self.criterion_recons_content(
            reconstruction, targets) * self.loss_coef_recons_content
        self.loss_latent_vector = self.criterion_latent_vector(
            latent_mu, latent_logvar) * self.loss_coef_latent_vector

        target_mask = torch.sum(targets, 1)
        target_mask[target_mask > 0.01] = 1
        target_mask[target_mask < 0.01] = 0

        self.loss_recons_mask = self.criterion_recons_mask(mask, target_mask) * \
                self.loss_coef_recons_mask

        if self.tv_loss_enable:
            self.loss_tv = tv_loss(predictions)
            return self.loss_recons_content + self.loss_latent_vector + self.loss_tv + self.loss_coef_recons_mask
        else:
            return self.loss_recons_content + self.loss_latent_vector + self.loss_recons_mask

    def reparameterize(self, mu, logvar):
        # VAE stuff
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std


class ReshapeLastDiv(nn.Module):
    def __init__(self, *args):
        super(ReshapeLastDiv, self).__init__()
        self.new_last_div_shape = args

    def forward(self, x):
        return x.view(x.shape[0], *self.new_last_div_shape)


class LossGaussianVAE(nn.Module):
    def forward(self, mu, logvar):
        # Based on pytorch example vae for gaussian distribution latent vector
        # see Appendix B from VAE paper:
        # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        # https://arxiv.org/abs/1312.6114
        # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
        return -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())


class UpsamplingResnetBlock(nn.Module):
    def __init__(self, c, c_out, norm_layer=nn.BatchNorm2d, use_dropout=False, use_bias=True):
        super(UpsamplingResnetBlock, self).__init__()

        layers = []
        layers += [
            nn.ConvTranspose2d(c, int(c / 2), kernel_size=3, stride=2, padding=1, output_padding=1, bias=use_bias),
            norm_layer(int(c / 2)), nn.ReLU(True), nn.ReflectionPad2d(1),
            nn.Conv2d(int(c / 2), c, kernel_size=3, padding=0, bias=use_bias), ]
        self.upsample = nn.Sequential(*layers)

        layers = []
        layers += [norm_layer(int(c)), nn.ReLU(True)]
        if use_dropout:
            layers += [nn.Dropout(0.5)]
        layers += [nn.ReflectionPad2d(1), nn.Conv2d(c, c_out, kernel_size=3, padding=0, bias=use_bias),
                   norm_layer(c_out)]
        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        y = self.upsample(x)
        y = y + self.conv(y)
        return y


if __name__ == "__main__":
    from torchsummary import summary

    model = AutoencoderVAE().to('cuda')
    summary(model, input_size=(3, 256, 256))
