#!/usr/bin/env bash

DATA_FOLDER_PATH=$1
if [ -z "$DATA_FOLDER_PATH" ]
then
    echo 'call with absolute path data folder as first parameter'
fi
docker pull lpasselin/fruitbox-processing-hdr
docker run -it --rm --volume $PWD:/opt/app \
    --user dockeruser:dockeruser -w=/home/dockeruser \
    --volume $DATA_FOLDER_PATH:/fruitbox-data \
    --name fruitbox-processing-hdr \
    lpasselin/fruitbox-processing-hdr python3 /opt/app/merge.py /fruitbox-data