import os
import subprocess
import sys
import logging
import datetime
# from tqdm import tqdm

# input folders, each contains N_LIGHTS folders of N_exposures ldr images.
#   ex: data/ldr/sample_number/light_number/exposure_number.jpg
# output folders: each folder contains N_LIGHTS hdr images {0.exr, 1.exr, 2.exr, ... j} where j=N_exposures
#   ex: data/hdr/sample_number/light_number.exr

logger = logging.getLogger('fruitbox-processing-hdr-merge')
logger.setLevel(logging.DEBUG)
fname_log = os.path.join(os.path.dirname(__file__),
                         datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S") + '.log')
fh = logging.FileHandler(fname_log)
fh.setLevel(logging.DEBUG)
logger.addHandler(fh)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)

if __name__ == "__main__":
    data_root = sys.argv[1]
    logger.info("Input parameter, data root folder: {}".format(data_root))

    data_root_ldr = os.path.join(data_root, "ldr")
    data_root_hdr = os.path.join(data_root, "hdr")
    if not os.path.exists(data_root_ldr) or not os.path.isdir(data_root_ldr):
        logger.critical("Input parameter folder (data root) should contain a folder called ldr")
    if not os.path.exists(data_root_hdr):
        os.mkdir(data_root_hdr)

    folders_ldr = (
        os.path.join(d_light, fname_expo)
        for d_light in (os.path.join(data_root_ldr, d) for d in os.listdir(data_root_ldr)
                        if os.path.isdir(os.path.join(data_root_ldr, d)))
        for fname_expo in os.listdir(d_light)
    )

    # TODO remove
    folders_ldr = list(folders_ldr)
    logger.info("folders_ldr size: {}".format(len(folders_ldr)))
    logger.info(folders_ldr)


    for d_ldr in folders_ldr:
        logger.debug("processing ldr directory: {}".format(d_ldr))
        out_path_hdr = d_ldr + '.exr'
        out_path_hdr = out_path_hdr.replace(data_root_ldr, data_root_hdr)

        if os.path.exists(out_path_hdr):
            logger.debug("hdr already exists: {}".format(out_path_hdr))
            continue

        filenames_ldr = [os.path.join(d_ldr, i) for i in os.listdir(d_ldr)]
        pfsinme_paths = ' '.join(filenames_ldr)

        d_hdr = os.path.split(out_path_hdr)[0]
        if not os.path.exists(d_hdr):
            logger.debug("creating hdr output dir: {} for images in {}".format(d_hdr, d_ldr))
            os.mkdir(d_hdr)

        logger.debug("pfstools cmd with pfsinme paths: {}".format(pfsinme_paths))
        cmd = "pfsinme {} | pfshdrcalibrate -x -r linear -v | pfsoutexr {}".format(pfsinme_paths, out_path_hdr)
        result = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
        logger.debug("pfstools call: {}".format(result.stdout))
