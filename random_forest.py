import argparse
import numpy as np
import torch
from torch.utils import data

from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor

from vae.net import AutoencoderVAE
from vae.train import TonemapLog, Resize, FruitMask
from dataset.dataset import CropFruitDataset

from pytorch_toolbox.transformations.compose import Compose

import matplotlib.pyplot as plt

import numpy as np
import matplotlib.pyplot as plt

from sklearn import svm, datasets
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Interpolation VAEFruit')
    parser.add_argument('-m', '--model', help="Model", required=False)
    parser.add_argument('-d', '--dataset', help="Dataset path", required=True)

    arguments = parser.parse_args()

    model_path = arguments.model
    data_path = arguments.dataset

    transformations = [Compose([
        Resize(0.50),
        FruitMask(inplace=True),
        TonemapLog(),
    ])]

    valid_dataset = CropFruitDataset(data_path, max_size=32, dataset_type="valid", transforms=transformations)
    valid_dataset.load()

    train_dataset = CropFruitDataset(data_path, max_size=32, dataset_type="train", transforms=transformations)
    train_dataset.load()

    if model_path:
        # Save
        net = AutoencoderVAE()
        net.load(model_path)
        net.eval()
        net.cuda()

        latent_space = {}
        for i, (img, (_, uid)) in enumerate(train_dataset):
            print(i)
            mu, _ = net.encoder(img.cuda().unsqueeze(0))
            latent_space[uid] = mu[0].detach().cpu().numpy()

        X_train = np.zeros((len(latent_space), 128))
        uids_train = np.zeros((len(latent_space)))
        for i, (uid, latent) in enumerate(latent_space.items()):
            X_train[i] = latent
            uids_train[i] = uid
        np.save('X_train', X_train)
        np.save('uids_train', uids_train)

        latent_space = {}
        for i, (img, (_, uid)) in enumerate(valid_dataset):
            print(i)
            mu, _ = net.encoder(img.cuda().unsqueeze(0))
            latent_space[uid] = mu[0].detach().cpu().numpy()

        X_valid = np.zeros((len(latent_space), 128))
        uids_valid = np.zeros((len(latent_space)))
        for i, (uid, latent) in enumerate(latent_space.items()):
            X_valid[i] = latent
            uids_valid[i] = uid
        np.save('X_valid', X_valid)
        np.save('uids_valid', uids_valid)
    else:
        # Load
        X_train = np.load('X_train.npy')
        uids_train = np.load('uids_train.npy')
        X_valid = np.load('X_valid.npy')
        uids_valid = np.load('uids_valid.npy')

        # Rendement
        Y_train = np.zeros_like(uids_train)
        for i, uid in enumerate(uids_train):
            uid = int(uid)
            Y_train[i] = train_dataset.metadata[uid][-1]
        X_train = X_train[Y_train != -1]
        Y_train = Y_train[Y_train != -1]

        Y_valid = np.zeros_like(uids_valid)
        for i, uid in enumerate(uids_valid):
            uid = int(uid)
            Y_valid[i] = train_dataset.metadata[uid][-1]
        X_valid = X_valid[Y_valid != -1]
        Y_valid = Y_valid[Y_valid != -1]


        # for depth in range(10, 32, 5):
        #     regr = RandomForestRegressor(max_depth=depth, random_state=0, n_estimators=100)
        #     regr.fit(X_train, Y_train)
        #     print(depth, regr.score(X_valid, Y_valid))

        regr = RandomForestRegressor(max_depth=30, random_state=0, n_estimators=200)
        regr.fit(X_train, Y_train)
        print(regr.score(X_valid, Y_valid))

        Y_pred = regr.predict(X_valid)

        Y_valid = np.around(Y_valid, decimals=2)
        bins = set(Y_valid)

        res = []
        for i in bins:
            res.append(Y_pred[Y_valid == i])

        # breakpoint()
        # res = []

        plt.figure(dpi=800)
        np.set_printoptions(precision=3)
        plt.boxplot(res, positions=list(bins), manage_xticks=False, widths=0.005)
        plt.xlabel('Vérité')
        plt.ylabel('Prédiction')
        plt.savefig('res', bbox_inches='tight')



