#!/usr/bin/env bash

# -e CUDA_VISIBLE_DEVICES=0 \

nvidia-docker run -it --rm --privileged --ipc=host \
    -v $PWD:/opt/app \
    -v $PWD/data/:/data/crop \
    -e PYTHONPATH="/opt/app:$PYTHONPATH" \
    --network="host" \
    --name lpasselin-train-test2 \
    -w="/opt/app" \
    -u $(id -u ${USER}):$(id -g ${USER}) \
    lpasselin/train \
    python vae/train.py \
    --output /opt/app/output \
    --dataset /data \
    --sharememory \
    --epoch 25 --batchsize 16 \
    --tensorboard --tensorboard_path /opt/app/logs


