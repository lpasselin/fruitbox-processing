import cv2
import numpy as np
import time
from ezexr import imread, imwrite

folderpath = "/home/louis/p/fruitbox/data/4"

images = np.array([
    # np.float32(imread("%s/0.exr" % folderpath)),
    # np.float32(imread("%s/1.exr" % folderpath)),
    # np.float32(imread("%s/2.exr" % folderpath)),
    # np.float32(imread("%s/3.exr" % folderpath)),
    imread("%s/0.exr" % folderpath),
    imread("%s/1.exr" % folderpath),
    imread("%s/2.exr" % folderpath),
    imread("%s/3.exr" % folderpath),
])


img1 = images[0]
gray_images = np.zeros((*img1.shape[0:2], 4))
gray_images[:,:,0] = np.mean(images[0], axis=2)
gray_images[:,:,1] = np.mean(images[1], axis=2)
gray_images[:,:,2] = np.mean(images[2], axis=2)
gray_images[:,:,3] = np.mean(images[3], axis=2)
final_img = np.zeros_like(img1)


def pixel_func(pixels, gray_pixels):
    darkest_pixel = np.argmin(gray_pixels)
    return np.array([(np.sum(pixels[:, 0]) - pixels[darkest_pixel, 0]) / 3,
                    (np.sum(pixels[:, 1]) - pixels[darkest_pixel, 1]) / 3,
                    (np.sum(pixels[:, 2]) - pixels[darkest_pixel, 2]) / 3,])


darkest_pixels = np.argmin(gray_images, axis=2)
brightest_pixels = np.argmax(gray_images, axis=2)


if True:
    images[0, darkest_pixels==0] = 0
    images[1, darkest_pixels==1] = 0
    images[2, darkest_pixels==2] = 0
    images[3, darkest_pixels==3] = 0
    images[0, brightest_pixels==0] = 0
    images[1, brightest_pixels==1] = 0
    images[2, brightest_pixels==2] = 0
    images[3, brightest_pixels==3] = 0
    final_img = np.sum(images, axis=0) / 2
    imwrite('%s/removed_brightest_and_darkest.exr' % folderpath, np.float16(final_img))

else:
    images[0, darkest_pixels==0] = 0
    images[1, darkest_pixels==1] = 0
    images[2, darkest_pixels==2] = 0
    images[3, darkest_pixels==3] = 0
    final_img = np.sum(images, axis=0) / 3
    imwrite('%s/removed_darkest.exr' % folderpath, np.float16(final_img))



"""
==================================================
Comparing edge-based and region-based segmentation
==================================================

In this example, we will see how to segment objects from a background. We use
the ``coins`` image from ``skimage.data``, which shows several coins outlined
against a darker background.
"""

import numpy as np
import matplotlib.pyplot as plt
from skimage import img_as_int
coins = img_as_int(np.mean(final_img, axis=2))

# ######################################################################
# # Edge-based segmentation
# # =======================
# #
# # Next, we try to delineate the contours of the coins using edge-based
# # segmentation. To do this, we first get the edges of features using the
# # Canny edge-detector.
#
# from skimage.feature import canny
#
#
# edges = canny(coins)
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(edges, cmap=plt.cm.gray, interpolation='nearest')
# ax.set_title('Canny detector')
# ax.axis('off')
#
# ######################################################################
# # These contours are then filled using mathematical morphology.
#
# from scipy import ndimage as ndi
#
# fill_coins = ndi.binary_fill_holes(edges)
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(fill_coins, cmap=plt.cm.gray, interpolation='nearest')
# ax.set_title('filling the holes')
# ax.axis('off')
#
#
# ######################################################################
# # Small spurious objects are easily removed by setting a minimum size for
# # valid objects.
#
# from skimage import morphology
#
# coins_cleaned = morphology.remove_small_objects(fill_coins, 21)
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(coins_cleaned, cmap=plt.cm.gray, interpolation='nearest')
# ax.set_title('removing small objects')
# ax.axis('off')
#
# ######################################################################
# # However, this method is not very robust, since contours that are not
# # perfectly closed are not filled correctly, as is the case for one unfilled
# # coin above.
# #
# # Region-based segmentation
# # =========================
# #
# # We therefore try a region-based method using the watershed transform.
# # First, we find an elevation map using the Sobel gradient of the image.
#
# from skimage.filters import sobel
#
# elevation_map = sobel(coins)
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(elevation_map, cmap=plt.cm.gray, interpolation='nearest')
# ax.set_title('elevation map')
# ax.axis('off')
#
# ######################################################################
# # Next we find markers of the background and the coins based on the extreme
# # parts of the histogram of gray values.
#
# markers = np.zeros_like(coins)
# markers[coins < 30] = 1
# markers[coins > 150] = 2
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(markers, cmap=plt.cm.nipy_spectral, interpolation='nearest')
# ax.set_title('markers')
# ax.axis('off')
#
# ######################################################################
# # Finally, we use the watershed transform to fill regions of the elevation
# # map starting from the markers determined above:
#
# segmentation = morphology.watershed(elevation_map, markers)
#
# fig, ax = plt.subplots(figsize=(4, 3))
# ax.imshow(segmentation, cmap=plt.cm.gray, interpolation='nearest')
# ax.set_title('segmentation')
# ax.axis('off')

######################################################################
# This last method works even better, and the coins can be segmented and
# labeled individually.

from skimage.color import label2rgb

segmentation = ndi.binary_fill_holes(segmentation - 1)
labeled_coins, _ = ndi.label(segmentation)
image_label_overlay = label2rgb(labeled_coins, image=coins)

fig, axes = plt.subplots(1, 2, figsize=(8, 3), sharey=True)
axes[0].imshow(coins, cmap=plt.cm.gray, interpolation='nearest')
axes[0].contour(segmentation, [0.5], linewidths=1.2, colors='y')
axes[1].imshow(image_label_overlay, interpolation='nearest')

for a in axes:
    a.axis('off')

plt.tight_layout()

plt.show()
