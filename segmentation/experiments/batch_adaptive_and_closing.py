import os
import numpy as np
from ezexr import imread, imwrite
import skimage
from skimage.color import rgb2hsv
from skimage.filters import threshold_local
from skimage.morphology import binary_closing
from skimage.io import imsave

import torch
import torch.nn
import torchvision

from matplotlib import pyplot as plt

adaptive_window = 2048-1
closing_size = 50
# adaptive_window = 511  # for img shape 720x1080
# closing_size = 8  # for img shape 720x1080


def threshold_mask(img):
    return img < threshold_local(img, block_size=adaptive_window, method='mean')


root = "/run/media/louis/LaCie/fruitbox-data/Photos/hdr/"
dirlist = reversed(sorted(list(os.listdir(root))))
for i in dirlist:
    imgdir = os.path.join(root, i)
    print('start: ', i)
    images = np.array([
        rgb2hsv(imread("%s/0.exr" % imgdir)[:,:,0:3])[:,:,2],
        rgb2hsv(imread("%s/1.exr" % imgdir)[:,:,0:3])[:,:,2],
        rgb2hsv(imread("%s/2.exr" % imgdir)[:,:,0:3])[:,:,2],
        rgb2hsv(imread("%s/3.exr" % imgdir)[:,:,0:3])[:,:,2],
    ])

    print('threshold ', i)
    b1 = threshold_mask(images[0].astype(np.float32))
    b2 = threshold_mask(images[1].astype(np.float32))
    b3 = threshold_mask(images[2].astype(np.float32))
    b4 = threshold_mask(images[3].astype(np.float32))
    b = b1 & b2 & b3 & b4

    print('closing ', i)

    b = skimage.morphology.binary_closing(b, skimage.morphology.diamond(closing_size))

    print('saving ', i)
    mask = b.astype(np.uint8) * 255
    imsave("%s/mask.png" % imgdir, mask)
