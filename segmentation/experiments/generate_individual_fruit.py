import cv2
import os
import numpy as np
import torch.nn.functional as F
import torch

from dataset.dataset import SegmentationFruitDataset, CropFruitDataset

if __name__ == "__main__":
    from matplotlib import pyplot as plt

    db = SegmentationFruitDataset("/mnt/usbdisk/fruitbox-data/Photos/hdr")
    total = len(db)

    max_size = 0
    min_margin = np.inf
    db2 = CropFruitDataset("/mnt/usbdisk/fruitbox-data/Photos/crop")
    rejected_folder = []
    for i in range(len(db)):
        dir = db.data_dirs[i]
        mask_path = os.path.join(dir, "mask.png")
        folder_id = dir.split('/')[-1]

        mask = cv2.imread(mask_path,0)
        ret,thresh = cv2.threshold(mask,245,255,0)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        cv2.drawContours(mask, contours, -1, 255, -1)

        corners = []
        for contour in contours:
            left = contour[:,:,0].min()//2
            right = contour[:,:,0].max()//2
            top = contour[:,:,1].min()//2
            bottom = contour[:,:,1].max()//2

            height = bottom-top
            length = right-left
            if right-left > 150 and bottom-top > 150 \
                    and right-left < 512 and bottom-top < 512:
                corners.append((left, right, top, bottom, height, length))

        imgs, _ = db[i]
        mask = torch.tensor(mask/255, dtype=torch.float)
        imgs = F.interpolate(imgs, scale_factor=0.5)
        mask = F.interpolate(mask.unsqueeze(0).unsqueeze(0), scale_factor=0.5)
        if len(corners) == 12:
            for j, (left, right, top, bottom, height, length) in enumerate(corners):
                top_padding = (512 - height)//2
                top -= top_padding
                bottom += (512 - height) - top_padding
                left_padding = (512 - length)//2
                left -= left_padding
                right += (512 - length) - left_padding

                top_padding, bottom_padding, left_padding, right_padding = (0, 0, 0, 0)
                if top < 0:
                    top_padding = abs(top)
                    top = 0
                if left < 0:
                    left_padding = abs(left)
                    left = 0
                max_bottom = mask.shape[2] - 1
                max_right = mask.shape[3] - 1
                if bottom > max_bottom:
                    bottom_padding = bottom - max_bottom
                    bottom = max_bottom
                if right > max_right:
                    right_padding = right - max_right
                    right = max_right

                imgs_res = F.pad(imgs[:,:,top:bottom,left:right], \
                        (left_padding, right_padding, top_padding, bottom_padding), mode='reflect')
                mask_res = F.pad(mask[:,:,top:bottom,left:right], \
                        (left_padding, right_padding, top_padding, bottom_padding), \
                        mode='constant').squeeze(0).squeeze(0)
                assert mask_res.shape == (512, 512)
                db2.add(imgs_res, mask_res, folder_id, j, (top, left))
            db2.save()
            print("{} completed ({}/{})".format(folder_id, (i+1), total))
            print("------")
        else:
            # plt.imshow(mask)
            # plt.show()
            rejected_folder.append(folder_id)
            print("{} skipped".format(folder_id))
            print("------")
    print(rejected_folder)

